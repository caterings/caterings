<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']],function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/total/order','HomeController@data_1')->name('data-1');

    Route::group(['middleware'=> 'role:admin|super-admin','prefix' => '/order'], function () {
        Route::get('/',                                         'OrderController@index')->name('order');
        Route::get('/all',                                      'OrderController@all')->name('order-all');
        Route::get('/create',                                   'OrderController@create')->name('order-create');
        Route::post('/store',                                   'OrderController@store')->name('order-store');
        Route::get('/edit/{id}',                                'OrderController@edit')->name('order-edit');
        Route::post('/update/{id}',                             'OrderController@update')->name('order-update');
        Route::get('/show/{id}',                                'OrderController@show')->name('order-show');
        Route::post('/status/update/{id}',                      'OrderController@updateStatus')->name('order-update-status');
    });

    Route::group(['middleware' => 'role:super-admin','prefix' => '/master-inventories'], function () {
        Route::get('/',                                         'InventoryController@index')->name('inventory');
        Route::get('/all',                                      'InventoryController@all')->name('inventory-all');
        Route::get('/create',                                   'InventoryController@create')->name('inventory-create');
        Route::post('/store',                                   'InventoryController@store')->name('inventory-store');
        Route::get('/edit/{id}',                                'InventoryController@edit')->name('inventory-edit');
        Route::post('/update/{id}',                             'InventoryController@update')->name('inventory-update');
        Route::delete('/delete',                                'InventoryController@destroy')->name('inventory-destroy');
    });

    Route::group(['middleware' => 'role:super-admin','prefix' => '/master-menu'], function () {
        Route::get('/',                                         'MenuController@index')->name('menu');
        Route::get('/all',                                      'MenuController@all')->name('menu-all');
        Route::get('/create',                                   'MenuController@create')->name('menu-create');
        Route::post('/store',                                   'MenuController@store')->name('menu-store');
        Route::get('/edit/{id}',                                'MenuController@edit')->name('menu-edit');
        Route::post('/update/{id}',                             'MenuController@update')->name('menu-update');
        Route::delete('/delete',                                'MenuController@destroy')->name('menu-destroy');
    });

    Route::group(['prefix' => '/users'], function () {
        Route::group(['middleware' => 'role:super-admin'], function () {
            Route::get('/',                                         'UserController@index')->name('users');
            Route::get('/all',                                      'UserController@getUsers')->name('user-all');
            Route::get('/create',                                   'UserController@create')->name('user-create');
            Route::post('/store',                                   'UserController@store')->name('user-store');
            Route::get('/show/{id}',                                'UserController@show')->name('user-show');
            Route::get('/edit/{id}',                                'UserController@edit')->name('user-edit');
            Route::post('/update/{id}',                             'UserController@update')->name('user-update');
            Route::delete('/destroy/{id}',                          'UserController@destroy')->name('user-destroy');
            Route::get('/avatar/{username}',                        'UserController@getAvatarUser')->name('user-avatar');
            Route::get('/email/verify/{id}',                        'UserController@sendEmailVerify')->name('user-verify');
        });
        Route::group(['middleware' => 'role:super-admin|admin'], function () {
            Route::get('/change/password',                          'UserController@changePassword')->name('user-change-password');
            Route::post('/change/password',                         'UserController@updatePassword')->name('user-change-password-update');
        });
    });

    Route::group(['middleware'=> 'role:super-admin','prefix' => '/roles'], function () {
        Route::get('/',                                         'RolesController@index')->name('roles');
        Route::get('/show',                                     'RolesController@getRoles')->name('role-all');
        Route::get('/show/role_users',                          'RolesController@modelRoles')->name('role-all-users');
        Route::get('/users/{id}',                               'RolesController@show')->name('role-user');
        Route::post('/update',                                  'RolesController@update')->name('role-user-update');
    });

    Route::group(['middleware'=> 'role:super-admin','prefix' => '/permissions'], function () {
        Route::get('/',                                         'PermissionController@index')->name('permissions');
    });

    Route::group(['middleware' => 'role:super-admin|admin', 'prefix' => '/inventory-event'], function () {
        Route::get('/',                                         'InventoryEventController@index')->name('inventory-event');
        Route::get('/all',                                      'InventoryEventController@allInventoryEvent')->name('inventory-event-all');
        Route::get('/get/{id}',                                 'InventoryEventController@getAllInventoryEvent')->name('inventory-event-all-by-event');
        Route::get('/show/{id}',                                'InventoryEventController@show')->name('inventory-event-show');
        Route::get('/create/{id}',                              'InventoryEventController@create')->name('inventory-event-create');
        Route::post('/store/{id}',                              'InventoryEventController@store')->name('inventory-event-store');
        Route::get('/edit/{id}',                                'InventoryEventController@edit')->name('inventory-event-edit');
        Route::post('/update/quantity',                         'InventoryEventController@update_quantity')->name('inventory-event-update-quantity');
        Route::post('/update/status',                           'InventoryEventController@updateStatus')->name('inventory-event-update-status');
        Route::get('/all/inventories',                          'InventoryController@all');
        Route::delete('/destroy',                               'InventoryEventController@destroy')->name('inventory-event-destroy');
    });

    Route::group(['middleware' => 'role:super-admin|admin', 'prefix' => '/finance'], function () {
        Route::get('/',                                         'FinanceController@index')->name('finance');
        Route::get('/all',                                      'FinanceController@all')->name('finance-all');
        Route::get('/show/{id}',                                'FinanceController@show')->name('finance-show');
        Route::get('/edit/{id}',                                'FinanceController@edit')->name('finance-edit');
        Route::post('/update/{id}',                             'FinanceController@update')->name('finance-update');
    });
});


Route::group(['middleware'=> 'auth', 'prefix'=>'/data'], function () {
    Route::get('/get/menus', 'MenuController@show');
});
