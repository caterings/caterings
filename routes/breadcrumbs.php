<?php
use App\User;


Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// ================ Orders ================
Breadcrumbs::for('order', function ($trail) {
    $trail->parent('home');
    $trail->push('Order', route('order'));
});
Breadcrumbs::for('order-create', function ($trail) {
    $trail->parent('order');
    $trail->push('New Orders', route('order-create'));
});
Breadcrumbs::for('order-edit', function ($trail, $post) {
    $trail->parent('order');
    $trail->push('Update Orders '.$post, route('order-edit', ['id'=> $post]));
});

// ================== Inventory Event =================
Breadcrumbs::for('inventory-event', function ($trail) {
    $trail->parent('home');
    $trail->push('View Inventory Event', route('inventory-event'));
});
Breadcrumbs::for('inventory-event-show', function ($trail, $post) {
    $trail->parent('inventory-event');
    $trail->push('View Inventory Event List '.$post, route('inventory-event-show', ['id'=> $post]));
});
Breadcrumbs::for('inventory-event-create', function ($trail, $post) {
    $trail->parent('inventory-event');
    $trail->push('Create Inventory Event '.$post, route('inventory-event-create', ['id'=> $post]));
});
Breadcrumbs::for('inventory-event-edit', function ($trail, $post) {
    $trail->parent('inventory-event');
    $trail->push('Update Inventory Event '.$post, route('inventory-event-edit', ['id'=> $post]));
});

// ============= Inventory ==============
Breadcrumbs::for('inventory', function ($trail) {
    $trail->parent('home');
    $trail->push('Inventories', route('inventory'));
});
Breadcrumbs::for('inventory-create', function ($trail) {
    $trail->parent('inventory');
    $trail->push('New Inventory', route('inventory-create'));
});
Breadcrumbs::for('inventory-edit', function ($trail, $post) {
    $trail->parent('inventory');
    $trail->push('Update Inventory '.$post->inventory_name, route('inventory-edit',['id'=> $post->id]));
});

// =============== Finance =================
Breadcrumbs::for('finance', function ($trail) {
    $trail->parent('home');
    $trail->push('Finance', route('finance'));
});

Breadcrumbs::for('finance-edit', function ($trail, $post) {
    $trail->parent('finance');
    $trail->push('Finance Edit', route('finance-edit',['id' => $post->id]));
});

// ============= Menu ==============
Breadcrumbs::for('menu', function ($trail) {
    $trail->parent('home');
    $trail->push('Menu', route('menu'));
});
Breadcrumbs::for('menu-create', function ($trail) {
    $trail->parent('menu');
    $trail->push('New Menu', route('menu-create'));
});
Breadcrumbs::for('menu-edit', function ($trail, $post) {
    $trail->parent('menu');
    $trail->push('Update Menu '.$post->inventory_name, route('menu-edit',['id'=> $post->id]));
});


// ================ USER ===================
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push('User Management', route('users'));
});
Breadcrumbs::for('user-create', function ($trail) {
    $trail->parent('users');
    $trail->push('New user', route('user-create'));
});
Breadcrumbs::for('user-edit', function ($trail, $post) {
    $trail->parent('users');
    $trail->push(ucfirst($post->username), url('/users/edit/'.$post->id));
});
Breadcrumbs::for('user-change-password', function ($trail) {
    $trail->parent('home');
    $trail->push('Change Password',  route('user-change-password'));
});

// ROles
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('home');
    $trail->push('Roles Management',  route('roles'));
});
Breadcrumbs::for('permissions', function ($trail) {
    $trail->parent('home');
    $trail->push('Permissions Management',  route('permissions'));
});

