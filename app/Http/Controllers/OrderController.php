<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Finance;
use App\Menu;
use App\Orders;
use App\Event;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.order.index');
    }

    /**
     * Display all data order
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $query = "select 
                    orders.id as order_id,
                    customers.customer_name,
                    events.id as event_id,
                    events.status as event_status,
                    CONCAT(events.event_start, ' s/d ', events.event_end) as event_date,
                    events.event_start,
                    events.event_end,
                case 
                    when 
                        DATEDIFF(events.event_end, CURDATE()) < 0
                        and events.status != 'done'
                    then 'due date'
                    else events.status
                end as order_status
                from orders
                left join events on orders.event_id = events.id
                left join customers on orders.customer_id = customers.id";
        $order = DB::select(DB::raw($query));
        if(!$order) return response()->json('no-content', 204);
        return response()->json($order);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'customer_name' => 'required',
            'customer_address' => 'required',
            'customer_phone' => 'required|min:12',
            'event_name' => 'required',
            'event_start' => 'required',
            'event_end' => 'required',
            'quantities' => 'required|min:1',
            'menus' => 'required',
        ]);

        if($validate->fails()) return response()->json($validate->errors(), 422);

        $lastOrder = DB::table('orders')->latest('id')->first();
        $count = ($lastOrder != null ? (int)substr($lastOrder->id, 3) : 1);
        $newORD_ID = 'ORD'.str_pad($count + 1,4,'0', STR_PAD_LEFT);

        $lastEvent = DB::table('events')->latest('id')->first();
        $countEvent = ($lastEvent != null ? (int)substr($lastEvent->id, 3) : 1);
        $newEVT_ID = 'EVT'.str_pad($countEvent + 1, 4, '0', STR_PAD_LEFT);

        $event = new Event();
        $event->id = $newEVT_ID;
        $event->event_name = $request->get('event_name');
        $event->event_place = $request->get('event_place');
        $event->event_start = $request->get('event_start');
        $event->event_end = $request->get('event_end');
        $event->status = 'ongoing';
        $event->save();

        $customer = new Customer();
        $customer->customer_name = $request->get('customer_name');
        $customer->customer_address = $request->get('customer_address');
        $customer->customer_email = $request->get('customer_email');
        $customer->customer_phone = $request->get('customer_phone');
        $customer->save();

        $new_customer_id = DB::table('customers')->latest('id')->first()->id;

        $order = new Orders();
        $order->id = $newORD_ID;
        $order->event_id = $newEVT_ID;
        $order->customer_id = $new_customer_id;
        $order->total_dp = str_replace(',','',$request->get('total_dp'));
        $order->total_price = $request->get('total_price');
        $order->save();

        $menus = $request->get('menus');
        $quantities = $request->get('quantities');

        for($i = 0 ; $i < count($menus); $i++){
            DB::table('order_menus')->insert([
                'order_id' => $newORD_ID,
                'menu_id' => $menus[$i],
                'quantity' => $quantities[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        $finance = new Finance();
        $finance->order_id = $newORD_ID;
        $finance->payment_date = $request->get('event_end');
        $finance->amount = 0;
        $finance->income = (str_replace(',','',$request->get('total_dp')) != 0
            ? 'Initial payment Rp.'.str_replace(',','',$request->get('total_dp'))
            : null);
        $finance->save();

        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Orders::find($id);
        if(!$order) abort(404);
        $event = Event::where('id',$order->event_id)->first();
        $customer = Customer::find($order->customer_id);
        $menus = DB::table('order_menus')
            ->leftJoin('menus', 'order_menus.menu_id', '=' ,'menus.id')
            ->where('order_menus.order_id', $id)
            ->get();
        return response()->json([
            'order'=>$order,
            'event' => $event,
            'customer' => $customer,
            'menus' => $menus
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders = DB::table('orders')
            ->leftJoin('customers','orders.customer_id','=','customers.id')
            ->leftJoin('events','events.id','=','orders.event_id')
            ->where('orders.id','=',$id)
            ->first();

        $daftar_menu = Menu::all();

        $menus = DB::table('order_menus')
            ->leftJoin('menus','menus.id','=','order_menus.menu_id')
            ->where('order_menus.order_id','=', $id)
            ->get();

        return view('pages.order.edit', compact('id','orders', 'menus', 'daftar_menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'customer_name' => 'required',
            'customer_address' => 'required',
            'customer_phone' => 'required|min:12',
            'event_name' => 'required',
            'event_start' => 'required',
            'event_end' => 'required',
            'quantities' => 'required|min:1',
            'menus' => 'required',
        ]);

        if($validate->fails()) return response()->json($validate->errors(),422);

        // Hitung total harga pesanan
        $total_price = 0;
        for ($i = 0 ;$i < count($request->get('menus')); $i++)
        {
            $menu = Menu::find($request->get('menus')[$i]);
            $total_price += $menu->menu_price * $request->get('quantities')[$i];
        }

        // Validasi apabila total dp lebih dari total harga pesanan
        if(str_replace(',','',$request->get('total_dp')) > $total_price)
            return response()->json(['Total DP' => 'Book payment cannot be more than the total menu price'], 422);

        $customer = Customer::find($request->get('customer_id'));
        $customer->customer_name = $request->get('customer_name');
        $customer->customer_email = $request->get('customer_email');
        $customer->customer_address = $request->get('customer_address');
        $customer->customer_phone = $request->get('customer_phone');
        $customer->save();

        $event = Event::find($request->get('event_id'));
        $event->event_name = $request->get('event_name');
        $event->event_place = $request->get('event_place');
        $event->event_start = $request->get('event_start');
        $event->event_end = $request->get('event_end');
        $end = Carbon::parse($request->get('event_end'));
        if ($end->diffInDays(Carbon::now()) >= 0) {
            $event->status = 'ongoing';
        }
        $event->save();

        DB::table('order_menus')->where('order_id', $id)->delete();
        for ($i = 0 ;$i < count($request->get('menus')); $i++)
        {
            DB::table('order_menus')->insert([
                'order_id' => $id,
                'menu_id' => $request->get('menus')[$i],
                'quantity' => $request->get('quantities')[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $order = Orders::find($id);
        $order->total_dp = str_replace(',','',$request->get('total_dp'));
        $order->total_price = $total_price;
        $order->save();

//        Finance::where('order_id',$order->id)->update([
//            'payment_date' => $request->get('event_end'),
//            'income' => (str_replace(',','',$request->get('total_dp')) == 0
//                ? null
//                : 'Initial Payment Rp.'.str_replace(',','',$request->get('total_dp')))
//        ]);

        $finance = Finance::where('order_id',$order->id)->first();
        $finance->payment_date = $request->get('event_end');
        $finance->income = $finance->income.' Update Payment Rp. '.$request->get('total_dp');
        $finance->save();

        return response()->json(['Success', 'Success updated Order '.$id],200);
    }


    public function updateStatus(Request $request, $id) {
        $orders = Orders::find($id);
        $event = Event::find($orders->event_id);
        if(!$event) return response()->json(['Error'=>'Event not Found'], 404);
        $event->status = $request->get('status');
        $event->save();
        return response()->json(['Success'=>'Success, Event Status updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
