<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use Notification;
use App\Notifications\PasswordNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Notifications\VerifyEmail;
use Image;
use Response;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'verified']); // This middleware allowed by users who have verified email, and must be login
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.master.users.index');
    }

    public function getUsers() {
        $users = User::all();
        if(!$users) return response()->json('no-content', 304); // 304 error code is No Content
        return response()->json($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation all form input
        $validate = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'fullname' => 'required',
            'email'    => 'required|unique:users,email',
            'gender'   => 'required',
            'file'    => 'image|max:5000',
        ]);
        if($validate->fails()) return response()->json($validate->errors(), 422); // catch error validation and return the errors messages

        $password = $this->password_generate(12); // generate random password as much 12 characters

        $image = ($request->hasFile('file') ? (string) Image::make($request->file('file'))->encode('jpg', 75) : null); // encode and convert to string as format JPG


        // Store to database
        $user = new User();
        $user->username = $request->get('username');
        $user->fullname = $request->get('fullname');
        $user->email = $request->get('email');
        $user->gender = $request->get('gender');
        $user->photo = $image;
        $user->password = bcrypt($password);
        $user->save();

        $user->assignRole('admin');

        Notification::send($user, new PasswordNotification($password)); // send new random password to email user

        return response()->json('Create new user successful, Password will be sent by email.', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id); // find user by id
        if(!$user) abort(404); // catch error if user doesn't exists
        return view('pages.master.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation all form input
        $validate = Validator::make($request->all(), [
            'fullname' => 'required',
            'email'    => 'required|unique:users,email,'.$id,
            'gender'   => 'required',
            'file'    => 'image|max:100000',
        ]);
        if($validate->fails()) return response()->json($validate->errors(), 422); // catch error validation and return the errors messages


        $image = ($request->hasFile('file') ? (string) Image::make($request->file('file'))->encode('jpg', 90) : ''); // encode and convert to string as format JPG


        // Store to database
        $user = User::find($id);
        $user->fullname = $request->get('fullname');
        $user->email = $request->get('email');
        $user->gender = $request->get('gender');
        if($image != '') $user->photo = $image;
        if($user->email != $request->get('email')) $user->email_verified_at = null;
        $user->save();

        return response()->json('Update user successful.', 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json('Delete user Success', 200);
    }


    /**
     * Generate random password
     * @param $chars
     * @return bool|string
     */
    public function password_generate($chars)
    {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($data), 0, $chars); // shuffle characters as much variable $chars
    }

    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvatarUser($username) {
        $user = User::where('username', $username)->first(); // FIND USER
        if(!$user) return response()->json('no-content', 204); // IF USER NOT FOUND
        $response = Response::make($user->photo);
        $response->header('Content-Type', 'image/JPEG');
        return $response;
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmailVerify($id) {
        $user = User::find($id); // find user by id
        if(!$user) abort(404); // catch if user is doesn't exists
        if($user->email_verified_at != null || $user->email_verified_at != '') return response()->json(['verified'=>'This email user has been verify'], 422); // check if email user has already verified
        Notification::send($user, new VerifyEmail()); // send email verify to email
        return response()->json('Success sent link verification email, Please check you email!',  200);
    }

    public function changePassword() {
        return view('pages.master.users.change-password');
    }

    public function updatePassword(Request $request) {
        $validate = Validator::make($request->all() , [
            'old_password' => 'required',
            'new_password' => 'required|same:confirm_new_password',
        ],[
            'old_password.required' => 'Old Password must be filled',
            'new_password.required' => 'New Password must be filled',
            'new_password.same'     => 'New Password and Confirm new password not matches!',
        ]);

        if($validate->fails()) return response()->json($validate->errors(), 422);


        $user = User::where('email', $request->get('email'))->first();
        if(!Hash::check($request->get('old_password'), $user->password)) return response()->json(['Old Password'=> 'Old Password is invalid'], 422);
        if(!$user) abort(404);
        if(Hash::check($request->get('new_password'),$user->password)) return response()->json(['New Password'=> 'Old Password and New Password cannot be same!'],422);

        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return response()->json('Success change new password',200);
    }
}
