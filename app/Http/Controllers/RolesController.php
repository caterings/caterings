<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('pages.master.roles.index', compact('roles'));
    }

    public function getRoles()
    {
        $roles = Role::all();
        if(!$roles) return response()->json('no-content',204);
        return response()->json($roles, 200);
    }
    public function modelRoles() {
        $user = User::role(['admin','super-admin'])->with('roles')->get();
        if(!$user) return response()->json('no-content', 204);
        return response()->json($user, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if(!$user) abort(404);
        return response()->json(['username'=>$user->username,'role_id' => $user->roles[0]->id],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->get('id'));
        if(!$user) abort(404);
        $new_role = Role::findById($request->get('roles'));
        $prev_roles = $user->getRoleNames();
        $user->removeRole($prev_roles[0]);
        $user->assignRole($new_role);
        return response()->json('Success change roles user', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
