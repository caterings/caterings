<?php

namespace App\Http\Controllers;

use App\Event;
use App\Finance;
use App\Inventory;
use App\InventoryEvent;
use App\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class InventoryEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.inventory-event.index');
    }

    public function check() {
        return view('pages.inventory-event.check-inventory-event');
    }

    public function allInventoryEvent() {
        $events = DB::table('orders')
            ->leftJoin('events','orders.event_id','=','events.id')
            ->leftJoin('customers', 'orders.customer_id','=','customers.id')
            ->select('events.id as event_id','orders.id as order_id','customers.customer_name')
            ->get();
        if(!$events) return response()->json('no-content', 204);
        return response()->json($events,200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllInventoryEvent($id) {
        $list_inventory = DB::table('inventory_lists')
            ->select('inventory_lists.*',
                'inventories.inventory_name',
                'inventories.inventory_quantity',
                'inventories.inventory_amount')
            ->leftJoin('inventories','inventory_lists.inventory_id','=','inventories.id')
            ->where('inventory_lists.event_id','=',$id)
            ->get();
        if(!$list_inventory) return response()->json('no-content',204);
        return response()->json($list_inventory, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $inventory = Inventory::all();
        return view('pages.inventory-event.create-ie', compact('id', 'inventory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required|unique:inventory_lists,inventory_id',
            'loan_quantity' => 'required|min:1',
        ],[
            'id.required' => 'Inventory must be filled',
            'id.unique' => 'Inventory already store in you Event',
        ]);

        if($validate->fails()) return response()->json($validate->errors(), 422);

        DB::table('inventory_lists')->insert([
            'event_id' => $id,
            'inventory_id' => $request->get('id'),
            'return_quantity' => 0,
            'loan_quantity' => $request->get('loan_quantity'),
            'status' => 'on borrowed',
            'charge' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        return response()->json($request->all(),200);
    }


    public function update_quantity(Request $request) {
        DB::table('inventory_lists')
            ->where('inventory_lists.id','=',$request->get('inventory_lists_id'))
            ->update([
                'loan_quantity' => $request->get('loan_quantity'),
                'return_quantity' => $request->get('return_quantity'),
                'status' => ($request->get('loan_quantity') == 0 ? 'done' : 'on borrowed'),
            ]);
        return response()->json('Success',200);
    }


    public function updateStatus(Request $request) {
        $inventory = InventoryEvent::find($request->get('id'));

        if($inventory->loan_quantity > 0) {
            $property = Inventory::find($inventory->inventory_id);
            $property->inventory_quantity = $property->inventory_quantity - $inventory->loan_quantity;
            InventoryEvent::where('id', $request->get('id'))
                ->update([
                    'charge' => $property->inventory_amount *  $inventory->loan_quantity,
                    'status' => 'done',
                ]);
            $property->save();
        }else {
            InventoryEvent::where('id', $request->get('id'))
                ->update([
                    'charge' => 0,
                    'status' => 'done',
                ]);
        }

        return response()->json('Success', 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inventory = Inventory::all();
        $list_inventory = DB::table('inventory_lists')
            ->leftJoin('inventories','inventory_lists.inventory_id','=','inventories.id')
            ->where('inventory_lists.event_id','=',$id)
            ->get();
        return view('pages.inventory-event.view-ie', compact('id','list_inventory','inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('inventory_lists')
            ->leftJoin('orders','orders.event_id','=','inventory_lists.event_id')
            ->leftJoin('customers','orders.customer_id','=','customers.id')
            ->leftJoin('inventories','inventories.id','=','inventory_lists.inventory_id')
            ->where('inventory_lists.id','=',$id)
            ->first();
        if(!$data) return response()->json('no-content',204);
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $loan = DB::table('inventory_lists')
            ->where('inventory_lists.id','=',$request->get('inventory_id'))
            ->where('event_id', '=',$request->get('event_id'))
            ->first();
//        $inventory = Inventory::find($loan->inventory_id);
//        $totalAmount = $inventory->inventory_amount * ($loan->loan_quantity + $loan->return_quantity);
//
//        $orders = Orders::where('event_id','=',$request->get('event_id'))->first();
//        $orders->total_price = $orders->total_price - $totalAmount;
//        $orders->save();

        // Update Loan
        DB::table('inventory_lists')
            ->where('inventory_lists.id','=',$request->get('inventory_id'))
            ->where('event_id','=',$request->get('event_id'))
            ->delete();


        return redirect()->back()->with('status','Inventory has been successful deleted from this event');
    }
}
