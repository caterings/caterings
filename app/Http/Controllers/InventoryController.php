<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.master.inventory.index');
    }

    public function all () {
        $inventories = Inventory::all();
        if(!$inventories) return response()->json('no-content',204);
        return response()->json($inventories, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master.inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'inventory_name' => 'required|unique:inventories,inventory_name',
            'inventory_quantity' => 'required|min:1',
            'inventory_amount' => 'required',
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();
        $inventory = new Inventory();
        $inventory->inventory_name = $request->get('inventory_name');
        $inventory->inventory_quantity = str_replace( ',','',$request->get('inventory_quantity'));
        $inventory->inventory_amount = str_replace(',','',$request->get('inventory_amount'));
        $inventory->save();

        return redirect()->route('inventory')->with('status','Success add new Inventory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Inventory::find($id);
        return view('pages.master.inventory.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'inventory_name' => 'required|unique:inventories,inventory_name,'.$id,
            'inventory_quantity' => 'required|min:1',
            'inventory_amount' => 'required',
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

        $inventory = Inventory::find($id);
        $inventory->inventory_name = $request->get('inventory_name');
        $inventory->inventory_quantity = $request->get('inventory_quantity');
        $inventory->inventory_amount = $request->get('inventory_amount');
        $inventory->save();

        return redirect()->route('inventory')->with('status','Inventory has been successful updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Inventory::find($request->get('inventory_id'))->delete();
        return redirect()->route('inventory')->with('status','Inventory hsa been successful deleted');
    }
}
