<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function data_1() {
        $order_done = Event::where('status','done')->count();
        $order_progress = Event::where('status', 'ongoing')->count();
        $order_canceled = Event::where('status','canceled')->count();
        $order_due_date = Event::where('status','due date')->count();

        return response()->json([
            'done' => $order_done,
            'progress' => $order_progress,
            'canceled' => $order_canceled,
            'due_date' => $order_due_date
        ],200);
    }
}
