<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Event;
use App\Finance;
use App\InventoryEvent;
use App\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.finance.index');
    }

    public function all() {
        $data = DB::table('orders')
            ->select('finances.id as id','orders.id as order_id','events.id as event_id','customers.customer_name','events.event_name')
            ->leftJoin('customers','customers.id','=','orders.customer_id')
            ->leftJoin('events','events.id','=','orders.event_id')
            ->leftJoin('finances', 'finances.order_id','=','orders.id')
            ->get();
        if(!$data) return response()->json('no-content',204);
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = "select 
                        finances.id as id,
                        orders.id as order_id,
                        events.id as event_id,
                        customers.customer_name,
                        events.event_name,
                        finances.payment_date,
                        finances.amount,
                        finances.income,
                        finances.outcome,
                        sum(charge)
                    from orders
                    left join events on orders.event_id = events.id
                    left join customers on orders.customer_id = customers.id
                    left join finances on finances.order_id = orders.id
                    left join inventory_lists on inventory_lists.event_id = events.id
                    where finances.id = $id
                    group by finances.id, orders.id, events.id, customer_name, event_name, finances.order_id, finances.payment_date, finances.amount, finances.income, finances.outcome";
        $finance = DB::select(DB::raw($query));
        if(!$finance) return response()->json('no-content', 204);
        return response()->json($finance[0], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $finance = Finance::find($id);
        $order = Orders::find($finance->order_id);
        $customer = Customer::find($order->customer_id);
        $event = Event::find($order->event_id);
        $charge = InventoryEvent::where('event_id',$order->event_id)->sum('charge');
        return view('pages.finance.edit', compact('finance','order', 'customer', 'event', 'charge'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $finance = Finance::find($id);
        $finance->payment_date = $request->get('payment_date');
        $finance->amount = str_replace(',','',$request->get('amount'));
        $finance->income = $request->get('income');
        $finance->outcome = $request->get('outcome');
        $finance->save();
        return redirect()->route('finance')->with(['status' => 'Success update finance']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
