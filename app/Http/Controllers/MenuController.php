<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.master.menus.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() {
        $menus = Menu::all();
        if(!$menus) return response()->json('no-content',204);
        return response()->json($menus, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master.menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'menu_name' => 'required|unique:menus,menu_name',
            'menu_price' => 'required',
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

        $menu = new Menu();
        $menu->menu_name = $request->get('menu_name');
        $menu->menu_price = str_replace(',','',$request->get('menu_price'));
        $menu->save();

        return redirect()->route('menu')->with('status','Success added new Menus');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        if(!$menu) return response()->json('no-content',204);
        return response()->json($menu->all(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu, $id)
    {
        $data = Menu::find($id);
        return view('pages.master.menus.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'menu_name' => 'required|unique:menus,menu_name,'.$id,
            'menu_price' => 'required',
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

        $menu = Menu::find($id);
        $menu->menu_name = $request->get('menu_name');
        $menu->menu_price = str_replace(',','',$request->get('menu_price'));
        $menu->save();

//        Update Total Price Orders
        $order_menus = DB::table('order_menus')
            ->leftJoin('menus','menus.id','=','order_menus.menu_id')
            ->where('order_menus.menu_id','=',$id)
            ->get();

        foreach ($order_menus as $key => $value)
        {
            $query = "select sum(new_price.total) as new_total_price from orders
                        join (
                            select menus.menu_price * order_menus.quantity as total from order_menus
                            inner join menus on menus.id = order_menus.menu_id
                        ) as new_price
                        WHERE orders.id = '".$value->order_id."'";
            $newPRICE = DB::select(DB::raw($query));
            Orders::where('id',$value->order_id)->update([
                'total_price' => $newPRICE[0]->new_total_price,
            ]);
        }

        return redirect()->route('menu')->with('status','Menu has been successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Menu::find($request->get('menu_id'))->delete();
        return redirect()->route('menu')->with('status','Menu has been successfully updated');
    }
}
