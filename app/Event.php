<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    public $incrementing = 'false';
    protected $fillable = [
        'id','event_name','event_place','event_start','event_end'
    ];
}
