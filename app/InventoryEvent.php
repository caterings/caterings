<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryEvent extends Model
{
    protected $table = 'inventory_lists';
    protected $primaryKey = 'id';
    protected $fillable = [
        'event_id','inventory_id','return_quantity','loan_quantity','status','charge'
    ];
}
