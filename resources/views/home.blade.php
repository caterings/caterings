@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Dashboard
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('home') }}
    </div>
@endsection



@section('content')
    <div class="container-fluid">
    <div class="row justify-content-center" id="total_data">
        <div class="col-md-3">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row align-items-center justify-content-start">
                    <div class="col-md-4 bg-success text-white">
                        <h2 class="display-4">-</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">Order Finished</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row align-items-center justify-content-start">
                    <div class="col-md-4 bg-primary text-white">
                        <h2 class="display-4">-</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">Order Progress</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row align-items-center justify-content-start">
                    <div class="col-md-4 bg-warning">
                        <h2 class="display-4">-</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">Order Canceled</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row align-items-center justify-content-start">
                    <div class="col-md-4 bg-danger text-white">
                        <h2 class="display-4">-</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">Order Due Date</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div id="chart_order"></div>
        </div>
        <div class="col-md-4">
            <div id="chart_order_donut"></div>
        </div>
    </div>
</div>
@endsection

@section('toast')

@endsection

@section('script')
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="{{asset('js/home.js')}}"></script>
@endsection
