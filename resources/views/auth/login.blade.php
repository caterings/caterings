@extends('layouts.app')

@section('content')
    <div class="fixed-top mt-3">
        <a href="{{url('/')}}" class="nav-link d-inline text-modern text-uppercase p-3 font-weight-bold" style="font-size: 13px;"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
<div class="container h-100">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-md-6">
            <div class="card">
                <img src="{{asset('img/caterings.png')}}" class="card-img-top w-75 mx-auto mt-4" alt="">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row justify-content-center">
                            <div class="col-md-7">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend rounded-0 ">
                                        <span class="input-group-text rounded-0 bg-modern" id="basic-addon1"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input id="username" type="text" class="form-control rounded-0 @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-7">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend rounded-0 ">
                                        <span class="input-group-text rounded-0 bg-modern" id="basic-addon1"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <input id="password" type="password" class="form-control rounded-0 @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                                    <div class="input-group-append rounded-0">
                                        <button class="btn btn-outline-modern rounded-0" type="button" id="button-addon2" onclick="showPassword(this)"><i class="fas fa-eye"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input class="custom-control-input" type="hidden" name="remember" id="remember" checked>

                        <div class="form-group mb-0">
                            <div class="col-md-6 offset-lg-3">
                                <button type="submit" class="btn btn-outline-modern rounded-0 btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link mx-auto d-block mt-2" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('toast')
    @if(!$errors->isEmpty())
        <div class="fixed-top">
            <!-- Flexbox container for aligning the toasts -->
            <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="min-height: 200px;">

                <!-- Then put toasts within -->
                <div class="toast" id="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <img src="{{asset('img/caterings.png')}}" width="20" class="rounded mr-2" alt="...">
                        <strong class="mr-auto">{{Config('app.name')}}</strong>
                        <small>{{\Carbon\Carbon::now()->format('D M Y')}}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-danger">
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('alertMessage')
    @if($errors->first())
        <p class="mb-0 pb-0">{{$errors->first()}}</p>
    @endif
@endsection

@section('style')
    <style>
        .form-control, .form-control:focus {
            border-color: #d65a31;
        }
    </style>
@endsection

@section('script')
    <script>
        function showPassword(e) {
            var type = $(e).parent().siblings('input[name="password"]').prop('type');
            $(e).parent().siblings('input[name="password"]').prop('type', (type == 'password' ? 'text' : 'password'));
        }
    </script>
@endsection
