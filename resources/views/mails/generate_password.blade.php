<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name','Laravel')}}</title>
</head>
<body>
    <h1>New Password</h1>
    <p>You new account has been created, on http://localhost:8000, Here is the password that we provided from the system : </p>
    <table>
        <tr>
            <td>Username</td>
            <th>: {{$username}}</th>
        </tr>
        <tr>
            <td>New Password</td>
            <th>: {{$password}}</th>
        </tr>
    </table>
    <p>Regard, <br> <b>{{config('app.name','Katering Online')}}</b></p>
</body>
</html>
