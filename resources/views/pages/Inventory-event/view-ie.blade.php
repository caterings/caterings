@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Inventory Event
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('inventory-event-show',$id) }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark d-flex justify-content-between"><h3 class="m-0">Inventory Event List</h3>
                        <button onclick="$('#modalAddIEList').modal('show');" class="btn btn-sm btn-outline-light"><i class="fas fa-plus-circle"></i> New Inventory {{$id}}</button>
                    </div>
                    <div class="card-body">
                        @if(count($list_inventory) == 0)
                            <div class="row justify-content-center align-items-center" style="height: 50vh">
                                <h1 class="text-center display-4 col-12 ">No Data Available...</h1>
                            </div>
                        @else
                            <table id="table_inventory_list" class="table table-sm responsive display" style="width: 100%"></table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="form-delete" action="{{route('inventory-event-destroy')}}" method="post">
        @csrf
        {{method_field('delete')}}
        <input type="hidden" name="event_id" value="{{$id}}">
        <input type="hidden" name="inventory_id">
    </form>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="modalAddIEList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Inventory Event List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event_id" value="{{$id}}">
                        <div class="form-group">
                            <label for="">Inventory</label>
                            <input type='text'
                                   placeholder='Select Inventory'
                                   class='form-control'
                                   data-search-in='inventory_name'
                                   data-visible-properties='["inventory_name","inventory_quantity"]'
                                   data-selection-required='true'
                                   data-value-property='["id","inventory_quantity"]'
                                   data-min-length='0'
                                   id='relative'
                                   name='inventory'>
                        </div>
                        <div class="form-group">
                            <label for="">Loan Quantity</label>
                            <input type="text" name="loan_quantity" class="form-control" placeholder="Load Quantity">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm rounded-0 btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-sm rounded-0 btn-primary" onclick="store();">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Info Inventory Event List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('inventory-event-update-quantity')}}" id="form-info" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="inventory_lists_id">
                        <div class="form-group row">
                            <label for="customer" class="col-sm-4 col-form-label">Customer</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" name="customer" id="customer">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="event_id" class="col-sm-4 col-form-label">Event ID</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" name="event_id" id="event_id">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inventory_name" class="col-sm-4 col-form-label">Inventory</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" name="inventory_name" id="inventory_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-4 col-form-label">Loan Quantity</label>
                            <div class="col-sm-8 d-flex align-items-center">
                                <input type="number" readonly class="form-control form-control-sm col-3" name="loan_quantity" id="loan_quantity_number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-4 col-form-label">Return Quantity</label>
                            <div class="col-sm-8 d-flex align-items-center">
                                <button type="button" class="btn btn-link" onclick="decrease_return_quantity(this);"><i class="fas fa-minus-circle"></i></button>
                                <input type="text" readonly class="form-control form-control-sm col-3" name="return_quantity">
                                <button type="button" class="btn btn-link" onclick="increase_return_quantity(this);"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm rounded-0 btn-primary" onclick="$('#form-info').submit();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('toast')
    @if(session('status'))
        <div class="fixed-top">
            <!-- Flexbox container for aligning the toasts -->
            <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="min-height: 200px;">

                <!-- Then put toasts within -->
                <div class="toast" id="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <img src="{{asset('img/caterings.png')}}" width="20" class="rounded mr-2" alt="...">
                        <strong class="mr-auto">{{Config('app.name')}}</strong>
                        <small>{{\Carbon\Carbon::now()->format('D M Y')}}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script src="{{asset('js/inventory-event/inventory-event-list.js')}}"></script>
@endsection
