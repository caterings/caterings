@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Menu
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('menu-edit', $data) }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @if(!$errors->isEmpty())
                {{$errors->first()}}
            @endif
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark d-flex justify-content-between"><h3 class="m-0">Update Inventory </h3>
                        <a href="{{route('menu')}}" class="btn btn-sm btn-outline-light"><i class="fas fa-chevron-circle-left"></i> back to menu</a>
                    </div>
                    <div class="card-body">
                        <form action="{{route('menu-update',['id'=>$data->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="menu_name" class="col-sm-2 col-form-label">Menu Name</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="menu_name" class="form-control @error('menu_name') is-invalid @enderror" id="menu_name" value="{{$data->menu_name}}" placeholder="Inventory Name" required autocomplete="menu_name" autofocus>
                                    @error('menu_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="menu_price" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="text"  name="menu_price" class="form-control @error('menu_price') is-invalid @enderror" id="menu_price" value="{{$data->menu_price}}" placeholder="Price" required autocomplete="menu_price" autofocus>
                                    </div>
                                    @error('menu_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary float-right">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('toast')
    @if(!$errors->isEmpty())
        <div class="fixed-top">
            <!-- Flexbox container for aligning the toasts -->
            <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="min-height: 200px;">

                <!-- Then put toasts within -->
                <div class="toast" id="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <img src="{{asset('img/caterings.png')}}" width="20" class="rounded mr-2" alt="...">
                        <strong class="mr-auto">{{Config('app.name')}}</strong>
                        <small>{{\Carbon\Carbon::now()->format('D M Y')}}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-danger">
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/master/menu/menu-edit.js')}}"></script>
@endsection
