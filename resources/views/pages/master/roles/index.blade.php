@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Roles Management
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('roles') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Roles</h3></div>
                    <div class="card-body">
                        <table id="table_roles" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark d-flex justify-content-between align-items-center">
                        <h4 class="mb-0">User Roles</h4>
                    </div>
                    <div class="card-body">
                        <table id="table_user_roles" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="modalRoles" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-dark text-white">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Change User Roles</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-update-roles" action="{{route('role-user-update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" name="username" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Roles</label>
                            <select name="roles" id="" class="form-control">
                                @foreach($roles as $data)
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="formUpdateSubmit();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/roles.js')}}"></script>
@endsection
