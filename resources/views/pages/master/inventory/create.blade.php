@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Inventory
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('inventory-create') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @if(!$errors->isEmpty())
                {{$errors->first()}}
            @endif
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark d-flex justify-content-between"><h3 class="m-0">New Inventory</h3>
                        <a href="{{route('inventory')}}" class="btn btn-sm btn-outline-light"><i class="fas fa-chevron-circle-left"></i> back to inventory</a>
                    </div>
                    <div class="card-body">
                        <form action="{{route('inventory-store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="inventory_name" class="col-sm-2 col-form-label">Inventory Name</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="inventory_name" class="form-control @error('inventory_name') is-invalid @enderror" id="inventory_name" value="{{old('inventory_name')}}" placeholder="Inventory Name" required autocomplete="inventory_name" autofocus>
                                    @error('inventory_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inventory_quantity" class="col-sm-2 col-form-label">Inventory Quantity</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="inventory_quantity" class="form-control @error('inventory_quantity') is-invalid @enderror" id="inventory_quantity" value="{{old('inventory_quantity')}}" placeholder="Inventory Quantity" required autocomplete="inventory_quantity" autofocus>
                                    @error('inventory_quantity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inventory_amount" class="col-sm-2 col-form-label">Inventory Amount</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="text"  name="inventory_amount" class="form-control @error('inventory_amount') is-invalid @enderror" id="inventory_amount" value="{{old('inventory_amount')}}" placeholder="Inventory Amount" required autocomplete="inventory_amount" autofocus>
                                    </div>
                                    @error('inventory_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary float-right">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('toast')
    @if(!$errors->isEmpty())
        <div class="fixed-top">
            <!-- Flexbox container for aligning the toasts -->
            <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="min-height: 200px;">

                <!-- Then put toasts within -->
                <div class="toast" id="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <img src="{{asset('img/caterings.png')}}" width="20" class="rounded mr-2" alt="...">
                        <strong class="mr-auto">{{Config('app.name')}}</strong>
                        <small>{{\Carbon\Carbon::now()->format('D M Y')}}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-danger">
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/master/inventory/inventory-create.js')}}"></script>
@endsection
