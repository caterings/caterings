@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Inventory
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('inventory') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark d-flex justify-content-between"><h3 class="m-0">Inventory</h3>
                        <a href="{{route('inventory-create')}}" class="btn btn-sm btn-outline-light"><i class="fas fa-plus-circle"></i> New Inventory</a>
                    </div>
                    <div class="card-body">
                        <table id="table_inventory" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{route('inventory-destroy')}}" method="post" class="d-none" id="form-destroy">
        @csrf
        {{method_field('delete')}}
        <input type="hidden" name="inventory_id">
    </form>
@endsection

@section('toast')
    @if(session('status'))
        <div class="fixed-top">
            <!-- Flexbox container for aligning the toasts -->
            <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="min-height: 200px;">

                <!-- Then put toasts within -->
                <div class="toast" id="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <img src="{{asset('img/caterings.png')}}" width="20" class="rounded mr-2" alt="...">
                        <strong class="mr-auto">{{Config('app.name')}}</strong>
                        <small>{{\Carbon\Carbon::now()->format('D M Y')}}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body text-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/master/inventory/inventory-list.js')}}"></script>
@endsection
