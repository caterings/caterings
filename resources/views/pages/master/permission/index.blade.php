@extends('layouts.app')

@section('content')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Permissions Management
    </div>

    <div class="container-fluid">
        {{ Breadcrumbs::render('permissions') }}
    </div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Permissions</h3></div>
                    <div class="card-body">
                        <table id="table_roles" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{--    <script src="{{asset('js/roles.js')}}"></script>--}}
@endsection
