@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Users Management
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('user-create') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white d-block align-middle">Create new users <a href="{{url('users')}}" id="back_to" class="float-right btn btn-sm btn-outline-light ml-auto mb-0"><i class="fas fa-chevron-circle-left"></i> Back</a></div>
                    <div class="card-body">
                        <form class="container-fluid" id="form-create-user" action="{{route('user-store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 col-sm-12 d-flex justify-content-center align-items-center">
                                    <div id="image-profile" class="d-block w-100">
                                        <img src="{{asset('img/user.png')}}" width="200" id="image-default" class="img-thumbnail p-5" alt="image not found" >
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="file" id="load-photo" class="mt-3 d-block" accept="image/*">
                                        <small class="invalid-feedback"></small>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="">Staff Username</label>
                                        <input type="text" class="form-control " name="username">
                                        <small class="invalid-feedback"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Staff Fullname</label>
                                        <input type="text" class="form-control" name="fullname">
                                        <small class="invalid-feedback"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Staff Email</label>
                                        <input type="email" class="form-control" name="email">
                                        <small class="invalid-feedback"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Staff Gender</label>
                                        <div class="form-group">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input class="custom-control-input" type="radio" name="gender" id="radio_male" value="male">
                                                <label class="custom-control-label" for="radio_male">Male</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input class="custom-control-input" type="radio" name="gender" id="radio_female" value="female">
                                                <label class="custom-control-label" for="radio_female">Female</label>
                                            </div>
                                        </div>
                                        <small class="invalid-feedback"></small>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer d-flex justify-content-end bg-white">
                        <button class="btn btn-primary" type="button" onclick="buttonStore();" id="btn-submit">
                            <span class="text-button">Save Changes</span>
                            <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .form-control, .form-control:focus{
            border: none;
            box-shadow: none;
            padding: 0;
            height: 23px;
        }
        .form-group{
            border: 1px solid #ced4da;
            -webkit-border-radius: .25rem;
            -moz-border-radius: .25rem;
            border-radius: .25rem;
            padding: .375rem .75rem;
        }
        .is-invalid{
            border: 1px solid #dc3545!important;
            border-radius: .25rem;
        }
    </style>
@endsection

@section('script')
    <script src="{{asset('js/vendor/load-image.all.min.js')}}"></script>
    <script src="{{asset('js/user.js')}}"></script>
@endsection
