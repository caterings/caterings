@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Change Password
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('user-change-password') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">Change Password</div>
                    <div class="card-body">
                        <form action="{{route('user-change-password-update')}}" method="post" enctype="multipart/form-data" id="form-change-password">
                            @csrf
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Old Password</label>
                                <div class="input-group mb-3 col-sm-10">
                                    <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password" aria-label="Old Password" aria-describedby="Old Password">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="old_password_toggle"><i class="fas fa-eye-slash"></i></button>
                                    </div>
                                </div>
                                <small id="old_password-text" class="offset-2 pl-3 text-danger" style="display: none;">CAPSLOCK</small>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">New Password</label>
                                <div class="input-group mb-3 col-sm-10">
                                    <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password" aria-label="New Password" aria-describedby="New Password">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="new_password_toggle"><i class="fas fa-eye-slash"></i></button>
                                    </div>
                                </div>
                                <small id="new_password-text" class="offset-2 pl-3 text-danger" style="display: none;">CAPSLOCK</small>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Confirm New Password</label>
                                <div class="input-group mb-3 col-sm-10">
                                    <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" aria-label="Confirm New Password" aria-describedby="Confirm New Password">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="confirm_new_password_toggle"><i class="fas fa-eye-slash"></i></button>
                                    </div>
                                </div>
                                <small id="confirm_new_password-text" class="offset-2 pl-3 text-danger" style="display: none;">CAPSLOCK</small>
                            </div>
                            <button class="btn btn-primary" type="button" onclick="formUpdate()">Save Changes</button>
                            <a href="{{url('/home')}}" class="btn btn-secondary">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/change-password.js')}}"></script>
@endsection
