@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Finance
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('finance-edit',$finance) }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Finance</h3></div>
                    <div class="card-body">
                        <form action="{{route('finance-update',['id'=>$finance->id])}}" method="post" enctype="multipart/form-data" class="row" id="form-finance-update">
                            @csrf
                            <div class="col-lg-4">
                                <table class="table table-sm table-striped table-bordered">
                                    <tr>
                                        <td width="150">Book ID</td>
                                        <th>{{$finance->order_id}}</th>
                                    </tr>
                                    <tr>
                                        <td width="150">Customer</td>
                                        <th>{{$customer->customer_name}}</th>
                                    </tr>
                                    <tr>
                                        <td width="150">Event</td>
                                        <th>{{$event->event_name}}</th>
                                    </tr>
                                    <tr>
                                        <td width="150">Event Place</td>
                                        <th>{{$event->event_place}}</th>
                                    </tr>
                                    <tr>
                                        <td>Total menu price</td>
                                        <th>Rp. {{number_format($order->total_price)}}</th>
                                    </tr>
                                    <tr>
                                        <td>Deposit</td>
                                        <th>Rp. {{number_format($order->total_dp)}}</th>
                                    </tr>
                                    <tr>
                                        <td>Charge Inventory</td>
                                        <th>Rp. {{number_format($charge)}}</th>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <th>Rp. {{number_format($order->total_price + $charge - $order->total_dp)}}</th>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Payment Date</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" readonly name="payment_date" value="{{$finance->payment_date}}">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button" id="openDate">Pick Date</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Payment Amount</label>
                                            <input type="text" name="amount" class="form-control" value="{{$finance->amount}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Income</label>
                                            <textarea name="income" id=""  class="form-control" placeholder="Income"  rows="5">{{$finance->income}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Outcoome</label>
                                            <textarea name="outcome" id=""  class="form-control" placeholder="Outcome"  rows="5">{{$finance->outcome}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary ml-auto" onclick="$('#form-finance-update').submit();">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/finance/finance-edit.js')}}"></script>
@endsection
