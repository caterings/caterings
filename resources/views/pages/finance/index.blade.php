@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Finance
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('finance') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Finance</h3></div>
                    <div class="card-body">
                        <table id="table_finance" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="modalFinanceInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Finance Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Book ID</label>
                        <div class="col-sm-9">
                            <input type="text" readonly class="form-control-sm form-control-plaintext">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Customer</label>
                        <div class="col-sm-9">
                            <input type="text" readonly class="form-control-sm form-control-plaintext">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Payment Date</label>
                        <div class="col-sm-9">
                            <input type="text" readonly class="form-control-sm form-control-plaintext">
                            <span class="badge badge-danger" id="labelPaymentDueDate">Due Date</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Amount</label>
                        <div class="col-sm-9">
                            <input type="text" readonly class="form-control-sm form-control-plaintext">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Income</label>
                        <div class="col-sm-9">
                            <textarea rows="10" class="form-control-sm form-control-plaintext"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label col-form-label-sm">Outcome</label>
                        <div class="col-sm-9">
                            <textarea rows="10" class="form-control-sm form-control-plaintext"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/finance/finance-list.js')}}"></script>
@endsection
