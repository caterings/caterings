@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Orders
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('order-edit', $id) }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Update Order</h3></div>
                    <div class="card-body">
                        <form class="row justify-content-center" id="form-create-orders" action="{{url('order/update/'.$id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-lg-6 col-sm-4 col-xs-12">
                                <h5 class="text-muted">Customer</h5>
                                <div class="border p-3 mb-3">
                                    <input type="hidden" name="customer_id" value="{{$orders->customer_id}}">
                                    <div class="form-group">
                                        <label for="customer_name">Customer Name</label>
                                        <input type="text" id="customer_name" name="customer_name" value="{{$orders->customer_name}}" class="form-control rounded-0" placeholder="Customer Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="customer_email">Customer Email</label>
                                        <input type="email" id="customer_email" name="customer_email" value="{{$orders->customer_email}}" class="form-control rounded-0" placeholder="Customer Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="customer_address">Customer Address</label>
                                        <textarea name="customer_address" id="customer_address" class="form-control rounded-0" cols="30" rows="5" placeholder="Customer Address">{{$orders->customer_address}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="customer_phone">Customer Phone</label>
                                        <input type="text" name="customer_phone" class="form-control rounded-0" id="customer_phone" value="{{$orders->customer_phone}}" placeholder="Customer Phone">
                                    </div>
                                </div>
                                <h5 class="text-muted">Event</h5>
                                <div class="border p-3 mb-3">
                                    <input type="hidden" name="event_id" value="{{$orders->event_id}}">
                                    <div class="form-group">
                                        <label for="">Event Name</label>
                                        <input type="text" name="event_name" class="form-control rounded-0" value="{{$orders->event_name}}" placeholder="Event Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Event Place</label>
                                        <textarea name="event_place" rows="5" class="form-control" placeholder="Event Place">{{$orders->event_place}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Event Date</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control rounded-0" placeholder="Event Start to Event End" value="{{$orders->event_start}} to {{$orders->event_end}}" readonly id="daterange" aria-label="Recipient's username" aria-describedby="button-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="button-pick-date">Pick a Date</button>
                                            </div>
                                            <input type="hidden" name="event_start" value="{{$orders->event_start}}">
                                            <input type="hidden" name="event_end" value="{{$orders->event_end}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-8 col-xs-12">
                                <h5 class="text-muted">Order</h5>
                                <a href="#" class="nav-link float-right" id="add_menu"><i class="fas fa-plus-circle"></i> Add Menu</a>
                                <div class="border p-3 mb-3">
                                    <div class="form-group" id="form-menus" data-length="{{count($menus)}}">
                                        <label for="">Menu</label>
                                        @foreach($menus as $data)
                                            <div class="row mb-1" id="wrap_menu_0">
                                                <div class="col-8">
                                                    <select id="menus_0" class="form-control rounded-0" name="menus[]" onchange="countPrice(this);" placeholder="Choose Menu" value="{{$data->menu_name}}">
                                                        @foreach($daftar_menu as $menu)
                                                            <option value="{{$menu->id}}" {{($data->menu_name == $menu->menu_name ? 'selected': null)}}>{{$menu->menu_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-2">
                                                    <input type="text" class="form-control rounded-0 quantity" name="quantities[]" min="1" placeholder="Quantity" value="{{$data->quantity}}" onkeyup="setQuantity(this);">
                                                </div>
                                                <button type="button" class="btn btn-sm btn-link col-1 btn-apply" onclick="applyMenu(this);" disabled><i class="fas fa-check"></i></button>
                                                <button type="button" class="btn btn-sm btn-link col-1" onclick="removeMenu(this);"><i class="fas fa-trash-alt"></i></button>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label for="">Total DP</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend rounded-0">
                                                <span class="input-group-text" id="basic-addon1">Rp.</span>
                                            </div>
                                            <input type="text" name="total_dp" class="form-control rounded-0" id="total_dp" value="{{$orders->total_dp}}" placeholder="Total DP">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary rounded-0 float-right" onclick="formSubmit();">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('css/vendor/multiple-select.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/daterangepicker.css')}}">
@endsection
@section('script')
    <script src="{{asset('js/vendor/multiple-select.min.js')}}"></script>
    <script src="{{asset('js/vendor/cleave.min.js')}}"></script>
    <script src="{{asset('js/vendor/cleave-phone.id.js')}}"></script>
    <script src="{{asset('js/vendor/moment.js')}}"></script>
    <script src="{{asset('js/vendor/jquery.daterangepicker.min.js')}}"></script>
    <script src="{{asset('js/update-order.js')}}"></script>
@endsection
