@extends('layouts.app')

@section('header')
    <div class="display-4 container-fluid" style="font-size: 2.5rem;">
        Orders
    </div>
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {{ Breadcrumbs::render('order') }}
    </div>
@endsection

@section('alert')
    <div class="container-fluid">
        <div id="alertError" class="alert alert-danger alert-dismissible rounded-0 fade show" role="alert">
            @yield('alertMessage')
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0">
                    <div class="card-header text-white bg-dark"><h3 class="m-0">Orders</h3></div>
                    <div class="card-body">
                        <table id="table_order" class="table table-sm responsive display" style="width: 100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row wrapInfo">
                    <div class="col-md-6">
                        <h4 class="bg-dark mb-0 p-2 text-white">Customer</h4>
                        <table class="table table-sm table-bordered table-striped align-items-center">
                            <tr>
                                <th>Name</th>
                                <td><b id="customer_name_info">a</b></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><b id="customer_email_info"></b></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td><b id="customer_address_info"></b></td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td><b id="customer_phone_info"></b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h4 class="bg-dark mb-0 p-2 text-white">Event</h4>
                        <table class="table table-sm table-bordered table-striped">
                            <tr>
                                <th>Name</th>
                                <td>: <b id="event_name_info"></b></td>
                            </tr>
                            <tr>
                                <th>Place</th>
                                <td>: <b id="event_place_info"></b></td>
                            </tr>
                            <tr>
                                <th>Date</th>
                                <td>: <b id="event_start_info"></b> until <b id="event_end_info"></b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <h4 class="bg-dark p-2 text-white">Menus</h4>
                        <table id="table_menus" class="table table-sm table-bordered responsive display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Booking ID</th>
                                <th>Menu</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th style="text-align:right">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm rounded-0 btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalChangeStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-sm table-striped" style="width: 100%">
                        <tr>
                            <th width="150">Book ID</th>
                            <td><span id="change_status_booking_id">-</span></td>
                        </tr>
                        <tr>
                            <th>Customer Name</th>
                            <td><span id="change_status_customer_name">-</span></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="status_ongoing" name="status" class="custom-control-input" value="ongoing" onclick="button_status_on_click(this)">
                                    <label class="custom-control-label" for="status_ongoing">ongoing</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="status_done" name="status" class="custom-control-input" value="done"  onclick="button_status_on_click(this)">
                                    <label class="custom-control-label" for="status_done">Done</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="status_due_date" name="status" class="custom-control-input" value="due date"  onclick="button_status_on_click(this)">
                                    <label class="custom-control-label" for="status_due_date">Due Date</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" onclick="updateStatus(this);">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('script')
    <script src="{{asset('js/list-order.js')}}"></script>
@endsection
