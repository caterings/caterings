<ul class="list-group list-group-flush">
    <li class="list-group-item {{(Request::is('home') ? ' active':'')}}">
        <a href="{{url('home')}}" class="nav-link" role="button" aria-expanded="false" aria-controls="book">
            <span><i class="fas fa-tachometer-alt"></i></span>
            Dashboard
        </a>
    </li>
    <li class="list-group-item {{(Request::is('order') ? ' active' : '')}}">
        <a class="nav-link " data-toggle="collapse" href="#booking" role="button" aria-expanded="false" aria-controls="book">
            <span><i class="fas fa-book"></i></span>
            Orders
            <div><i class="fas fa-caret-right"></i></div>
        </a>
        <div class="collapse " id="booking">
            <ul class="list-group list-group-flush text-center text-white">
                <li class="list-group-item">
                    <a href="{{url('order/create')}}" class="nav-link"><i class="fas fa-plus-circle"></i> Add Order</a>
                </li>
                <li class="list-group-item">
                    <a href="{{url('order')}}" class="nav-link"><i class="fas fa-list-alt"></i> List Order</a>
                </li>
            </ul>
        </div>
    </li>
    <li class="list-group-item {{(Request::is('inventory-event') ? ' active':'')}}">
        <a href="{{route('inventory-event')}}" class="nav-link" role="button" aria-expanded="false" aria-controls="book">
            <span><i class="far fa-list-alt"></i></span>
            Recheck Inventory Event
        </a>

    </li>
    <li class="list-group-item {{(Request::is('/finance?') ? 'active':'')}}">
        <a href="{{route('finance')}}" class="nav-link">
            <span><i class="fas fa-landmark"></i></span>
            Finance
        </a>
    </li>
    @role('super-admin')

        <li class="list-group-item {{(Request::is('/role?') ? 'active':'')}}">
            <a class="nav-link" data-toggle="collapse" href="#roles_permission" role="button" aria-expanded="false" aria-controls="roles_permission">
                <span><i class="fas fa-key"></i></span>
                Data Master
                <div><i class="fas fa-caret-right"></i></div>
            </a>
            <div class="collapse " id="roles_permission">
                <ul class="list-group list-group-flush text-left text-white" >
                    <li class="list-group-item">
                        <a href="{{url('users')}}" class="nav-link">User Management</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('roles')}}" class="nav-link">Master Roles</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('inventory')}}" class="nav-link">Master inventory</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('menu')}}" class="nav-link">Master Menu</a>
                    </li>
                </ul>
            </div>
        </li>
    @endrole
</ul>
