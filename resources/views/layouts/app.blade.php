<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.0/css/select.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('css/vendor/multiple-select.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/jquery.flexdatalist.css')}}">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    @yield('style')
</head>
<body>
    <div id="app">
        @if(!Request::is('login'))
            <nav class="navbar navbar-expand-md navbar-white bg-white shadow-sm fixed-top">
                <div class="container-fluid">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{asset('img/caterings-logo.png')}}" width="48" class="d-inline-block align-top" alt="">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span><i class="fas fa-user-circle"></i></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                {{--@if (Route::has('register'))--}}
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                                    {{--</li>--}}
                                {{--@endif--}}
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->fullname }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right rounded-0 border-0" id="" aria-labelledby="navbarDropdown">
                                        <a href="{{route('user-change-password')}}" class="dropdown-item">Change Password</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        @endif

        @if(Auth::guest())
            <div id="alertError" class="alert alert-danger alert-dismissible fixed-top rounded-0 fade show" role="alert">
                @yield('alertMessage')
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container-fluid {{(Request::is('login') ? 'mt-0' : 'mt-5')}}">
                <div class="row align-items-stretch flex-wrap" style="min-height: 100vh">
                    <main class="py-4 col-12">
                        @yield('toast')
                        @yield('content')
                    </main>
                </div>
            </div>
        @else
            <div class="container-fluid mt-5">
                <div class="row align-items-stretch flex-wrap" style="min-height: 100vh">
                    <div class="col-2 pl-0 pr-4 pt-4 shadow-sm bg-white collapse show navbar-collapse" id="sidebar">
                        @include('partials.sidebar')
                    </div>
                    <main class="py-4 col-10">
                        @yield('toast')
                        @yield('header')
                        @yield('breadcrumbs')
                        @yield('alert')
                        @yield('content')
                    </main>
                </div>
            </div>
        @endif
    </div>

    @yield('modal')

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="{{asset('js/vendor/multiple-select.min.js')}}"></script>
    <script src="{{asset('js/vendor/cleave.min.js')}}"></script>
    <script src="{{asset('js/vendor/cleave-phone.id.js')}}"></script>
    <script src="{{asset('js/vendor/moment.js')}}"></script>
    <script src="{{asset('js/vendor/jquery.daterangepicker.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery.flexdatalist.min.js')}}"></script>
    <script src="{{asset('js/vendor/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/layout.js')}}"></script>
    @yield('script')
</body>
</html>
