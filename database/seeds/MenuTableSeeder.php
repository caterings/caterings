<?php

use Illuminate\Database\Seeder;
use App\Menu;
class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu();
        $menu->menu_name = 'Paket 1';
        $menu->menu_price = 20000;
        $menu->save();

        $menu = new Menu();
        $menu->menu_name = 'Paket 2';
        $menu->menu_price = 30000;
        $menu->save();

        $menu = new Menu();
        $menu->menu_name = 'Paket 3';
        $menu->menu_price = 40000;
        $menu->save();
    }
}
