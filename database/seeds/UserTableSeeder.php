<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->username = 'superadmin';
        $user->fullname = 'Super Admin';
        $user->gender = 'male';
        $user->email = 'muharramsyah19@gmail.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('superadmin');
        $user->save();

        $user->assignRole('super-admin');
    }
}
