var table_order,
    modalChangeStatus = $('#modalChangeStatus'),
    table_info_menu,
    on_click_status = '',
    modalInfo = $('#modalInfo'),
    config = {
    responsive : true,
    columns: [
        {
            data:null,
            title : 'No.',
            render : function(data, type, row, meta) {
                return meta.row +1;
            }
        },
        {
            data : 'order_id',
            title : 'Booking ID',
            className : 'font-weight-bold',
            render : function(data, type, row) {
                return data.toUpperCase();
            }
        },
        {
            data : 'customer_name',
            title : 'Customer',
        },
        {
            data : 'event_date',
            title : 'Event Date',
        },
        {
            data : 'order_status',
            title : 'Status',
            render: function (data, type, row) {
                if(data === 'ongoing') return '<button type="button" class="btn btn-link p-0" onclick="changeStatusEvent(this);"><span class="badge badge-primary">ongoing</span></button>';
                else if(data === 'done') return '<button type="button" class="btn btn-link p-0" onclick="changeStatusEvent(this);"><span class="badge badge-success">Done</span></button>';
                else return '<button type="button" class="btn btn-link p-0" onclick="changeStatusEvent(this);"><span class="badge badge-danger">Due Date</span></button>';
            }
        },
        {
            data : 'order_id',
            title : 'Action',
            render : function (data, type, row) {
                var id = data;
                return '<button href="#" class="btn btn-sm btn-outline-dark"  data-value="'+data+'" onclick="GETINFO(this)"><i class="fas fa-info-circle"></i></button>  ' +
                    '<a href="/order/edit/'+data+'" class="btn btn-sm btn-outline-dark"><i class="far fa-edit"></i></a>';
            }
            
        }
    ]
},
    config_info_menu = {
        responsive : true,
        columns: [
            {
                data:null,
                render : function(data, type, row, meta) {
                    return meta.row +1;
                }
            },
            {
                data : 'order_id',
                className : 'font-weight-bold',
                render : function(data, type, row) {
                    return data.toUpperCase();
                }
            },
            {
                data : 'menu_name',
            },
            {
                data : 'menu_price',
                render : function (data, type, row) {
                    return 'Rp.'+data.toLocaleString('en')+' pcs';
                }
            },
            {
                data : 'quantity',
            },
            {
                data : 'total_price',
                render : function (data, type,row) {
                    return 'Rp.'+data.toLocaleString('en');
                }
            }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
        
            // Total over all pages
            total = api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
            total_price = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
        
            // Update footer
            $( api.column( 3 ).footer() ).html(
                'Rp. '+total.toLocaleString('en')
            );
            $( api.column( 5 ).footer() ).html(
                'Rp. '+total_price.toLocaleString('en')
            );
        }
    };


$(document).ready(function () {
    table_order = $('#table_order').DataTable(config);
    table_info_menu = $('#table_menus').DataTable(config_info_menu);
    var response = load('/order/all');
    
    table_order.clear().draw();
    table_order.rows.add(response);
    table_order.columns.adjust().draw();
    
    // modalInfo.on('shown.bs.modal', function () {GETINFO()});
    modalInfo.on('hidden.bs.modal', function () {resetInfoORDER('.wrapInfo')});
});



function load(url) {
    var all_data;
    $.ajax({
        url : window.location.origin+url,
        method : 'get',
        async : false,
        success:function (response) {
            all_data = response;
        }
    });
    return all_data;
}

function changeStatusEvent (e) {
    var row = $(e).parents('tr');
    modalChangeStatus.modal('show');
    var order_id = $(row).find('td').eq(1).text(),
        customer_name = $(row).find('td').eq(2).text(),
        status = $(e).children().text();
    
    $('#change_status_booking_id').text(order_id);
    $('#change_status_customer_name').text(customer_name);
    on_click_status = status;
    if(status === 'ongoing') {
        $('input[name="status"]').eq(0).prop('checked',true);
    }else if(status === 'Done'){
        $('input[name="status"]').eq(1).prop('checked',true);
    }else {
        $('input[name="status"]').eq(2).prop('checked',true);
    }
}

function button_status_on_click(e) {
    on_click_status = e.value;
}

function updateStatus() {
    $.ajax({
        url : window.location.origin+'/order/status/update/'+$('#change_status_booking_id').text(),
        method: 'post',
        data : {
            _token : $('meta[name="csrf-token"]').attr('content'),
            status : on_click_status,
        },
        success: function (response) {
            swal(Object.keys(response)+'' , response[Object.keys(response)]+'', 'success');
        },
        error : function (data) {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
            swal(name, value_error +'','error');
        }
    }).done(function () {
        // Load data
        var response = load('/order/all');
        table_order.clear().draw();
        table_order.rows.add(response);
        table_order.columns.adjust().draw();
        
        // Close Modals
        modalChangeStatus.modal('hide');
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function GETINFO(e) {
    var id = $(e).data('value'),
        all_data;
    $.ajax({
        url : window.location.origin+'/order/show/'+id,
        method : 'get',
        async: false,
        success: function(response) {
            all_data = response;
        }
    }).done(function () {
        if(all_data != null)
            filledInfoORDER('.wrapInfo', all_data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function filledInfoORDER (selector, data) {
    var wrap = $(selector),
        child = wrap.find('b'),
        menus = wrap.find('.menus');
    
    console.log(data.customer.customer_name);
    child.eq(0).text(data.customer.customer_name);
    child.eq(1).text(data.customer.customer_email);
    child.eq(2).text(data.customer.customer_address);
    child.eq(3).text(data.customer.customer_phone);
    child.eq(4).text(data.event.event_name);
    child.eq(5).text(data.event.event_place);
    child.eq(6).text(data.event.event_start);
    child.eq(7).text(data.event.event_end);
    
    var allMenus = [];
    
    data.menus.forEach(function (key, index) {
        allMenus.push({
            order_id : key.order_id,
            menu_name : key.menu_name,
            menu_price : key.menu_price,
            quantity : key.quantity,
            total_price : key.menu_price * key.quantity
        });
    });

    table_info_menu.clear().draw();
    table_info_menu.rows.add(allMenus);
    table_info_menu.columns.adjust().draw();
    
    modalInfo.modal('show');
}

function resetInfoORDER(selector) {

    var wrap = $(selector),
        child = wrap.find('b'),
        menus = wrap.find('.menus');
    
    $.each(child, function (e) {
        $(this).text('');
    });
}


