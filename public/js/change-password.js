$(document).ready(function (){
    
    toggleShowPassword('old_password_toggle');
    toggleShowPassword('new_password_toggle');
    toggleShowPassword('confirm_new_password_toggle');
    
    capslock_detection('old_password');
    capslock_detection('new_password');
    capslock_detection('confirm_new_password');
    
    
    $('#form-change-password').on('submit', function (e) {
        e.preventDefault();
        SEND_XHR(this);
    })
});

function toggleShowPassword(idSelector) {
    $('#'+idSelector).on('click', function () {
        var formInput = $(this).parent().siblings(),
            type = $(formInput).prop('type');
        $(formInput).prop('type', (type === 'password' ? 'text' : 'password'));
        if($(this).find('i').hasClass('fa-eye-slash')) $(this).find('i').toggleClass('fa-eye-slash fa-eye');
        else $(this).find('i').toggleClass('fa-eye fa-eye-slash');
    });
}

function capslock_detection (idSelector) {
    var input = document.getElementById(idSelector),
        text = document.getElementById(idSelector+'-text');
    input.addEventListener('keyup', function (e) {
        text.style.display = (e.getModifierState("CapsLock") ? 'block' : 'none');
    });
}

function formUpdate() {
    $('#form-change-password').submit();
}

function SEND_XHR(obj) {
    var action = obj.action,
        method = obj.method;
    
    $.ajax({
        url : action,
        method : method,
        data : $(obj).serialize(),
        success:function (response) {
            swal('Change Password', response+', You must be re-login', 'success')
            .then((value) => {
                document.getElementById('logout-form').submit(); // WILL BE RE-LOGIN
            });
        },
        error: function (data) {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
        
            swal(name, value_error+'','error');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}
