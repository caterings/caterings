var menus_length=1,
    orderMenu = [],
    totalPrice = 0;

$(document).ready(function () {
    
    new Cleave('#customer_phone', {
        phone : true,
        phoneRegionCode : 'ID',
        prefix: '+62',
    });
    new Cleave('#total_dp', {
        numeral: true,
    });
    
    new Cleave('.quantity',  {
        numeral: true,
    });
    
    $('#daterange').dateRangePicker({
        autoClose: true,
        separator: ' to ',
        startDate: moment().format(),
    }).bind('datepicker-change',function(event,obj)
    {
        /* This event will be triggered when second date is selected */
        $(this).val(obj.value);
        $('input[name="event_start"]').val(formatDate(obj.date1));
        $('input[name="event_end"]').val(formatDate(obj.date2));
    });
    
    $('#button-pick-date').click(function(evt)
    {
        evt.stopPropagation();
        $('#daterange').data('dateRangePicker').open();
    });
    
    var new_menus = load('/data/get/menus');
    $('#menus_0').append('<option selected disabled>Choose Menu</option>');
    new_menus.forEach(function (key, val) {
        $('#menus_0').append('<option value="'+key.id+'" data-price="'+key.menu_price+'">'+key.menu_name+'</option>');
    });
    
    $('#add_menu').click(function (evt) {
        evt.stopPropagation();
        var newMenus = '<div class="row mb-1">\n' +
            '<div class="col-8">\n' +
            '    <select id="menus_'+(menus_length)+'" class="form-control rounded-0" onchange="countPrice(this);"  name="menus[]" placeholder="Choose Menu"></select>\n' +
            '</div>\n' +
            '<div class="col-2">\n' +
            '    <input type="text" class="form-control rounded-0" name="quantities[]" placeholder="Quantity" value="1" onkeyup="setQuantity(this);">\n' +
            '</div>\n' +
            '<button type="button" class="btn btn-sm btn-link col-1 btn-apply" onclick="applyMenu(this);" disabled><i class="fas fa-check"></i></button>\n' +
            '<button type="button" class="btn btn-sm btn-link col-1" onclick="removeMenu(this);"><i class="fas fa-trash-alt"></i></button>\n' +
            '   </div>';
        
        $('#form-menus').append(newMenus);
        
        // INITIAL DEFAULT OPTION
        $('#menus_'+menus_length).append('<option selected disabled>Choose Menu</option>');
        
        new_menus.forEach(function (key, val) {
            $('#menus_'+menus_length).append('<option value="'+key.id+'" data-price="'+key.menu_price+'">'+key.menu_name+'</option>');
        });
        menus_length++;
    });
    
});

function formSubmit() {
    var el = document.getElementById('form-create-orders'),
        action = el.action,
        method = el.method,
        inputs = $(el).serialize();
    
    $.ajax({
        url : action,
        method : method,
        data : inputs,
        success: function (response) {
            swal('Success', 'Successful added new ORDER','success');
        },
        error: function (data) {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
            
            swal(name, value_error+'','error');
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    
    return [year, month, day].join('-');
}

function load(url) {
    var all_data;
    $.ajax({
        url : window.location.origin+url,
        method : 'get',
        async : false,
        success:function (response) {
            all_data = response;
        }
    });
    return all_data;
}

function removeMenu(event) {
    var id = $(event).siblings('.col-8').find('select').prop('id');
    var menu_selected = $(event).siblings('.col-8').find('select').val();
    var menu_quantity = $(event).siblings('.col-2').find('input').val();
    orderMenu.find(function (obj,i) {
        if(obj.id === id) orderMenu.splice(i, 1);
    });
    var temp_total_price = 0;
    for(let i = 0; i< orderMenu.length; i++) {
        var total_price = orderMenu[i].menu_price * orderMenu[i].menu_quantity;
        temp_total_price+=total_price;
    }
    totalPrice = temp_total_price;
    $('#total_price').find('b').text('Rp. '+temp_total_price.toLocaleString('en'));
    $('input[name="total_price"]').val(temp_total_price);
    $(event).parent().remove();
}

function applyMenu(obj) {
    var id = $(obj).siblings('.col-8').find('select').prop('id');
    var menu_selected = $(obj).siblings('.col-8').find('select').val();
    var menu_price = $(obj).siblings('.col-8').find('select option[value="'+menu_selected+'"]').data('price');
    var menu_quantity = $(obj).siblings('.col-2').find('input').val();
    var isExists = orderMenu.find(key => key.id === id);
    if(isExists) {
        orderMenu.find(function (evt, i) {
            if(evt.id === id) {
                evt.menu_id = menu_selected;
                evt.menu_price = menu_price;
                evt.menu_quantity = parseInt(menu_quantity);
            }
        });
    }else {
        orderMenu.push({
            id : id,
            menu_id : menu_selected,
            menu_price : menu_price,
            menu_quantity : parseInt(menu_quantity),
        })
    }
    $(obj).prop('disabled', true);
    var temp_total_price = 0;
    for(let i = 0; i< orderMenu.length; i++) {
        var total_price = orderMenu[i].menu_price * orderMenu[i].menu_quantity;
        temp_total_price+=total_price;
    }
    totalPrice = temp_total_price;
    $('#total_price').find('b').text('Rp. '+temp_total_price.toLocaleString('en'));
    $('input[name="total_price"]').val(temp_total_price);
}
function countPrice(obj) {
    $(obj).parent().siblings('button.btn-apply').prop('disabled', false);
}

function setQuantity(obj) {
    $(obj).parent().siblings('button.btn-apply').prop('disabled', false);
}


