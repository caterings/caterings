var table_roles,
    table_roles_users,
    form_update_roles = document.getElementById('form-update-roles');
$(document).ready(function () {
    table_roles = $('#table_roles').DataTable({
        responsive: true,
        columns : [
            {
                data : null,
                title : 'No.',
                render : function(data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data : 'name',
                title : 'Roles Name',
            }
        ]
    });
    table_roles_users = $('#table_user_roles').DataTable({
        columns : [
            {
                data : null,
                title : 'No.',
                render : function(data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data : 'username',
                title : 'Users',
            },
            {
                data : 'roles',
                title : 'Roles',
                render: function (data, type, row) {
                    return '<span class="badge badge-primary">'+data[0].name+'</span>';
                }
            },
            {
                data : 'id',
                title : 'Action',
                render : function (data, type,row) {
                    return '<button type="button" class="btn btn-link p-0" onclick="updateRoles('+data+')"><i class="fas fa-user-edit"></i></button>'
                }
            }
        ]
    });
    loadAll();
    loadUserRoles();
    
    $('#form-update-roles').on('submit', function (e) {
        e.preventDefault();
        update(this);
    })
});

function loadAll() {
    $.ajax({
        url : window.location.origin+'/roles/show',
        method : 'get',
        success: function (response) {
            table_roles.clear().draw();
            table_roles.rows.add(response);
            table_roles.columns.adjust().draw();
        },
    });
}

function loadUserRoles() {
    $.ajax({
        url : window.location.origin+'/roles/show/role_users',
        method : 'get',
        success: function (response) {
            table_roles_users.clear().draw();
            table_roles_users.rows.add(response);
            table_roles_users.columns.adjust().draw();
        },
    });
}


function updateRoles(id) {
    $('#modalRoles').modal('show');
    var elements = form_update_roles.elements;
    $.ajax({
        url : window.location.origin+'/roles/users/'+id,
        method: 'get',
        success:function (response) {
            elements.id.value = id;
            elements.username.value = response.username;
            elements.roles.value = response.role_id;
        }
    });
}


function formUpdateSubmit() {
    $('#form-update-roles').submit();
}

function update(el) {
    $.ajax({
        url : el.action,
        method : el.method,
        data : $(el).serialize(),
        success:function (response) {
            swal('Success', response+'','success');
        }
    }).done(function () {
        loadUserRoles();
        $('#modalRoles').modal('hide');
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}
