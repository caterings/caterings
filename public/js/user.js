var table_user, source_image_default;
$(document).ready(function() {
    table_user = $('#table_users').DataTable({
       responsive : true,
       columns : [
           {
               data : null,
               title: 'No.',
               render : function(data, type, full, meta) {
                   return meta.row + 1;
               }
           },
           {data : 'username',title: 'Username'},
           {data : 'fullname',title: 'Fullname'},
           {
               data : 'email', title: 'Email Address',
           },
           {
               data : 'email_verified_at',
               title : 'Verified',
               className: 'text-center',
               render : function(data, type, row){
                   return (data != null ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-times-circle text-danger"></i>') ;
               }
           },
           {
               data : 'gender',
               title: 'Gender',
               render : function(data, type, row){
                   return data.charAt(0).toUpperCase() + data.slice(1) ;
               }
           },
           {
               data : 'id',
               title: 'Action',
               render : function (data, type, row) {
                   return '<a href="users/edit/'+data+'"><i class="fas fa-user-edit"></i></a> / <button type="button" class="btn btn-link p-0" onclick="destroy('+data+')"><i class="fas fa-user-times"></i></button>'
               }
           },
       ]
   });
    
    loadUser();
    
    if(window.location.pathname === '/users/create') {
       var image_default = document.getElementById('image-default');
       source_image_default = (image_default ? image_default.src : null);
       document.getElementById('load-photo').onchange = function (e) {
           var wrapImage = document.getElementById('image-profile');
           while (wrapImage.firstChild) {
               wrapImage.removeChild(wrapImage.firstChild); // REMOVE PREVIOUS IMAGE
           }
           var loadingImage = loadImage(
               e.target.files[0],
               function (img) {
                   var newImage = wrapImage.appendChild(img); // APPEND NEW IMAGE
                   newImage.classList.add('img-thumbnail'); // ADD CLASS BOOTSTRAP
               },
               {maxWidth: 200} // SET IMAGE SIZE
           );
           if (!loadingImage) {
               // alert('Error, While load image maybe is broken, Try again!');
               var imageDefault = document.createElement('img');
               imageDefault.src = source_image_default;
               imageDefault.width = 200;
               imageDefault.classList.add('img-thumbnail');
               imageDefault.classList.add('p-5');
               wrapImage.appendChild(imageDefault);
           }
       };
   }
    $('input').on('keyup',function () {
        $(this).parents('.form-group').removeClass('is-invalid');
        $(this).parents('.form-group').find('.invalid-feedback').removeClass('d-block');
    });
    
    $('input[type=radio]').on('click', function () {
        $(this).parents('.form-group').removeClass('is-invalid');
        $(this).parents('.form-group').find('.invalid-feedback').removeClass('d-block');
    });
    
    $('#form-create-user').on('submit', function(e) {
        e.preventDefault();
        SEND_XHR(this);
    });
    
    $('#form-update-user').on('submit', function (e) {
        e.preventDefault();
        SEND_XHR(this);
    })
    
});


function loadUser() {
    $.ajax({
        url : window.location.origin+'/users/all',
        method :'get',
        success: function (response) {
            table_user.clear().draw();
            table_user.rows.add(response);
            table_user.columns.adjust().draw();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function buttonStore() {
    $('#form-create-user').submit();
}
function buttonUpdate() {
    $('#form-update-user').submit();
}

function SEND_XHR(obj) {
    var formData = new FormData($(obj)[0]);
    $.ajax({
        url : obj.action,
        method : obj.method,
        processData: false,
        contentType: false,
        data : formData,
        beforeSend:function () {
            $('#btn-submit').prop('disabled',true);
            $('#btn-submit span.text-button').text('Loading...');
            $('#btn-submit span.spinner-border').removeClass('d-none');
        },
        complete:function () {
            $('#btn-submit').prop('disabled', false);
            $('#btn-submit span.text-button').text('Save Changes');
            $('#btn-submit span.spinner-border').addClass('d-none');
        },
        success: function (response) {
            swal('Success', response+'','success');
            setTimeout(function () {
                $('#back_to').trigger('click');
            },2000);
        },
        error: function (data) {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
            
            swal(name, value_error+'','error');
            
            for(var obj in errors)
            {
                var parent = $('input[name="'+obj+'"]').parents('.form-group');
                parent.addClass('is-invalid');
                parent.find('.invalid-feedback').text(errors[obj][0]);
                parent.find('.invalid-feedback').addClass('d-block');
            }
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function verifiedEmail(id) {
    $.ajax({
        url : window.location.origin+'/users/email/verify/'+id,
        method : 'get',
        success:function (response) {
            swal('Email Verify', 'Link has been sent, Please check you email!', 'success');
        },
        error: function (data) {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
    
            swal(name, value_error+'','error');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function destroy(id)
{
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this user !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : window.location.origin+'/users/destroy/'+id,
                method : 'delete',
                data : {_token : $('meta[name="csrf-token"]').attr('content') },
                success:function (response) {
                    swal('Success',response+'','success');
                }
            }).done(function () {
                loadUser();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if(jqXHR.status !== 422)
                    swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
            });
        }
    });
    
}
