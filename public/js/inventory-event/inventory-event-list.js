var table,
    invetory_selected = '',
    inventory_loadn = '',
    max_loan_quantity = 0,
    config = {
        columns : [
            {
                data:null,
                title : 'No.',
                render : (data, type, row, meta) => {
                    return meta.row +1;
                }
            },
            {
                data : 'order_id',
                title : 'Book ID',
            },
            {
                data : 'event_id',
                title: 'Event ID'
            },
            {
                data : 'customer_name',
                title : 'Customer Name',
            },
            {
                data : 'event_id',
                title : 'Action',
                render : (data, type, row) => {
                    return '<a href="'+window.location.origin+'/inventory-event/show/'+data+'" class="btn btn-sm btn-outline-dark"><i class="fas fa-info-circle"></i></a>'
                },
            }
        ]
    };

var table_inventory_list,
    config_inventory_list = {
    columns : [
        {
            data:null,
            title : 'No.',
            render : (data, type, row, meta) => {
                return meta.row +1;
            }
        },
        {
            data : 'event_id',
            title: 'Event ID'
        },
        {
            data : 'inventory_name',
            title : 'Inventory',
        },
        {
            data : 'loan_quantity',
            title : 'Loan Inventory Quantity'
        },
        {
            data : 'return_quantity',
            title : 'Return Inventory Quantity',
        },
        {
            data : 'status',
            title : 'Status Inventory',
            render : (data, type, row) => {
                return (data === 'done' ? '<span class="badge badge-success">'+data+'</span>' : '<span class="badge badge-warning">'+data+'</span>')
            },
        },
        {
            data : 'charge',
            title : 'Charge',
            render : (data, type, row) => {
                if(row.status === 'on borrowed') {
                    return '<span class="badge badge-primary">No Charge</span>';
                } else {
                    return (data === 0 ? '<span class="badge badge-primary">No Charge</span>' : 'Rp. '+data.toLocaleString('en'));
                }
            }
        },
        {
            data : 'id',
            title : 'Action',
            render : (data, type, row) => {
                if(row.status === 'done') {
                    return '<button onclick="updateIE('+data+')" class="btn btn-sm btn-outline-dark"><i class="fas fa-info-circle"></i></button> <button type="button" onclick="destroy('+data+')" class="btn btn-sm btn-outline-dark"><i class="fas fa-trash"></i></button>'
                }else {
                    return '<button class="btn btn-sm btn-outline-dark" onclick="checkedInventory('+data+','+row.loan_quantity+')"><i class="fas fa-check-circle"></i></button> <button onclick="updateIE('+data+')" class="btn btn-sm btn-outline-dark"><i class="fas fa-info-circle"></i></button> <button type="button" onclick="destroy('+data+')" class="btn btn-sm btn-outline-dark"><i class="fas fa-trash"></i></button>'
                }
            },
        }
    ]
};
$(document).ready(() => {
    table = $('#table_inventory_event').DataTable(config);
    table_inventory_list = $('#table_inventory_list').DataTable(config_inventory_list);
    load();
    getAllInventory_list();
    
    $('input[name="inventory"]').flexdatalist({
        minLength: 0,
        valueProperty: 'id',
        selectionRequired: true,
        visibleProperties: ["inventory_name","inventory_quantity"],
        searchIn: 'inventory_name',
        data: get_inventories(),
    });
    
    var cleave = new Cleave($('input[name="loan_quantity"]'), {
        numeral: true,
    });
    
    $('input[name="loan_quantity"]').on('keyup', function (e) {
        if(this.value > max_loan_quantity) {
            alert('The maximum loan cannot be more than 40');
            this.value = 0;
            return false;
        }else {
            return true;
        }
    });
    
    $('input[name="inventory"]').on('change:flexdatalist', function(event, set, options) {
        invetory_selected = JSON.parse(set.value).id;
        max_loan_quantity = JSON.parse(set.value).inventory_quantity;
        $('input[name="loan_quantity"]').prop('max',max_loan_quantity);
    });
    
    $('#modalAddIEList').on('hidden.bs.modal', () => {
       invetory_selected = '';
       max_loan_quantity = 0;
    });

    $('#modalInfo').on('hidden.bs.modal', () => {
        document.getElementById('#form-info').reset();
    });

    $('#form-info').on('submit', function (e) {
        e.preventDefault();
        updateQuantity(this);
    })
});

function load() {
    $.ajax({
        url : window.location.origin +'/inventory-event/all',
        method : 'get',
        success : (response) => {
            table.clear().draw();
            table.rows.add(response);
            table.columns.adjust().draw();
        }
    });
}

function getAllInventory_list() {
    $.ajax({
        url : window.location.origin + '/inventory-event/get/'+$('input[name="event_id"]').val(),
        method : 'get',
        success: (response) => {
            table_inventory_list.clear().draw();
            table_inventory_list.rows.add(response);
            table_inventory_list.columns.adjust().draw();
        }
    })
}

function get_inventories () {
    var all_data;
    $.ajax({
        url : window.location.origin + '/inventory-event/all/inventories',
        method : 'get',
        async : false,
        success : (response) => {
            all_data = response;
        }
    });
    
    return all_data;
}

function store() {
    $.ajax({
        url : window.location.origin + '/inventory-event/store/'+$('input[name="event_id"]').val(),
        method: 'post',
        data : {
            _token : $('meta[name="csrf-token"]').attr('content'),
            id : invetory_selected,
            loan_quantity : $('input[name="loan_quantity"]').val(),
        },
        success: (response) => {
            swal('Success', 'Inventory has been store in this Event','success');
        },
        error : (response) => {
            var errors = data.responseJSON,
                key = Object.keys(errors)[0].replace('_',' '),
                name = key.charAt(0).toUpperCase() + key.slice(1),
                value_error = errors[Object.keys(errors)[0]];
    
            swal(name, value_error+'','error');
        }
    }).done(()=> {
        $('#modalAddIEList').modal('hide');
        window.location.reload();
    }).fail((jqXHR, textStatus, errorThrown) => {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function updateIE(id) {
    $('#modalInfo').modal('show');
    $.ajax({
        url : window.location.origin + '/inventory-event/edit/'+id,
        method : 'get',
        success: (response) => {
            var form = document.getElementById('form-info').elements;
            form.inventory_lists_id.value = id;
            form.customer.value = response.customer_name;
            form.event_id.value = response.event_id;
            form.inventory_name.value = response.inventory_name;
            form.return_quantity.value = response.return_quantity;
            form.return_quantity.max = response.loan_quantity + response.return_quantity;
            form.loan_quantity.value = response.loan_quantity;
        },
    });
}

function decrease_return_quantity(e) {
    var input = $(e).siblings('input'),
        value = $(input).val(),
        loan = $('input[name="loan_quantity"]').val();
    if (parseInt(value) > 0) {
        value--;
        loan++;
    }
    $('input[name="loan_quantity"]').val(loan);
    $(input).val(value);
}

function increase_return_quantity(e) {
    var input = $(e).siblings('input'),
        loan = document.getElementById('loan_quantity_number').value,
        max = $(input).prop('max'),
        value = $(input).val();

    if (parseInt(value) < max){
        value++;
        loan--;
    }

    $('input[name="loan_quantity"]').val(loan);
    $(input).val(value);
}

function updateQuantity(e) {
    $.ajax({
        url : e.action,
        method : e.method,
        data : $(e).serialize(),
        success : (response) => {
            swal("Success update quantity inventory")
            .then((value) => {
                window.location.reload();
            });
        }
    }).done( () => {
    })
}

function destroy(id) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this inventory from your event!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $('#form-delete').find('input[name="inventory_id"]').val(id);
            $('#form-delete').submit();
        }
    });
}


function checkedInventory(id, loan_quantity) {
    if(loan_quantity > 0) {
        swal({
            title: "Are you sure?",
            text: "inventory has not all been returned, will you be charged additional fees ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                changeStatus(id);
            }
        });
    }else {
        changeStatus(id);
    }
}

function changeStatus (id) {
    $.ajax({
        url : window.location.origin + '/inventory-event/update/status',
        method : 'post',
        data : {
            _token  : $('meta[name="csrf-token"]').attr('content'),
            id : id
        },
        success: function (response) {
            window.location.reload();
        }
    })
}
