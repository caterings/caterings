$(document).ready(() => {
    var cleave = new Cleave('#inventory_quantity', {
        numeral: true,
    });
    var cleave = new Cleave('#inventory_amount', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
});
