var table,
    config = {
        responsive: true,
        columns: [
            {
                data:null,
                title : 'No.',
                render : function(data, type, row, meta) {
                    return meta.row +1;
                }
            },
            {
                data : 'menu_name',
                title : 'Menu',
            },
            {
                data : 'menu_price',
                title : 'Price',
                render: function (data, type, row) {
                    return 'Rp.'+data.toLocaleString('en');
                }
            },
            {
                data : 'id',
                title : 'Action',
                render: function (data, type,row){
                    return '<a href="/master-menu/edit/'+data+'" class="btn btn-sm btn-outline-dark"><i class="fas fa-edit"></i></a> <button type="button" class="btn btn-sm btn-outline-dark" onclick="destroy('+data+')"><i class="fas fa-trash"></i></button> '
                }
            }
        ]
    };
$(document).ready(() => {
    table = $('#table_menus').DataTable(config);
    load();
});

function load() {
    $.ajax({
        url : window.location.origin+'/master-menu/all',
        method : 'get',
        success: function (response) {
            table.clear().draw();
            table.rows.add(response);
            table.columns.adjust().draw();
        },
    });
}

function destroy(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this menu !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $('#form-destroy').find('input[name="menu_id"]').val(id);
            $('#form-destroy').submit();
        }
    });
    
}
