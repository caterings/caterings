var chart, chart_donut;
var config_spline = {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Monthly Orders',
    },
    subtitle: {
        text: 'Source: Caterings.com'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: 'Incomes'
        },
        labels: {
            formatter: function () {
                return this.value + '°';
            }
        }
    },
    credits: {
        enabled : false,
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    }
};
var config_donut = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Monthly Orders'
    },
    credits: {
        enabled : false,
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    }
};
$(document).ready(function () {
    chart = new Highcharts.chart('chart_order',config_spline);
    chart_donut = new Highcharts.chart('chart_order_donut', config_donut);
    loadData_1();
});


function loadData_1() {
    var wrap = $('#total_data').find('h2.display-4');
    $.ajax({
        url : window.location.origin + '/total/order',
        method : 'get',
        success : function (response) {
            console.log(response);
            wrap[0].innerHTML = response.done;
            wrap[1].innerHTML = response.progress;
            wrap[2].innerHTML = response.canceled;
            wrap[3].innerHTML = response.due_date;
        },
    })
}
