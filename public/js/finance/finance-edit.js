$(document).ready(()=>{
    var cleave = new Cleave($('input[name="amount"]'), {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    
    $('input[name="payment_date"]').dateRangePicker({
        singleDate : true,
        autoClose : true,
        showShortcuts: false,
        startDate: moment().format('YYYY-MM-DD'),
    });
    
    $('#openDate').click(function(evt) {
        evt.stopPropagation();
        $('input[name="payment_date"]').data('dateRangePicker').open();
    });
});
