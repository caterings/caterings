var table,
    modalInfo = $('#modalFinanceInfo');
    config = {
        responsive : true,
        columns : [
            {
                data : null,
                title: 'No.',
                render: (data, type,row, meta)=>{
                    return meta.row+1
                }
            },
            {
                data : 'order_id',
                title : 'Book ID',
            },
            {
                data : 'customer_name',
                title : 'Customer',
            },
            {
                data : 'event_id',
                title : 'Event ID',
            },
            {
                data : 'event_name',
                title : 'Event'
            },
            {
                data : 'id',
                title : 'Action',
                render : (data, type, row) => {
                    return '<button type="button" href="#" class="btn btn-sm btn-outline-dark" onclick="getInfo('+data+')"><i class="fas fa-info-circle"></i></button> <a href="finance/edit/'+data+'" class="btn btn-sm btn-outline-dark"><i class="fas fa-edit"></i></a>';
                }
            }
        ]
    };
$(document).ready(()=>{
    table = $('#table_finance').DataTable(config);
    load();
});

function load() {
    $.ajax({
        url : window.location.origin + '/finance/all',
        method : 'get',
        success : (response) => {
            table.clear().draw();
            table.rows.add(response);
            table.columns.adjust().draw();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if(jqXHR.status !== 422)
            swal("Error " + jqXHR.status, textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ', ' + errorThrown, "error");
    });
}

function getInfo(id) {
    $.ajax({
        url : window.location.origin + '/finance/show/'+id,
        method : 'get',
        success : (response) => {
            console.log(response);
            modalInfo.modal('show');
            
            var inputs = modalInfo.find('input'),
                textarea = modalInfo.find('textarea'),
                labelDuedate = modalInfo.find("#labelPaymentDueDate"),
                a = moment(response.payment_date),
                b = moment(),
                diff = a.diff(b,'days');
            
            inputs[0].value = response.order_id;
            inputs[1].value = response.customer_name;
            inputs[2].value = a.format('ll');
            inputs[3].value = 'Rp. '+response.amount;
            textarea[0].value = (response.income == null ? 'No Income' : response.income);
            textarea[1].value = (response.outcome == null ? 'No Outcome' : response.outcome);
            (diff < 0 ? labelDuedate.show() : labelDuedate.hide());
        }
    });
}
